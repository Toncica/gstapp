-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 01, 2018 at 08:14 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `getsettransport`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `id_transport` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sender` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `message`, `title`, `id_transport`, `created_at`, `sender`) VALUES
(1, ' prihva?am Vaš upit za prijevoz.', 'odobreno', 1, '2018-06-30 19:12:47', 1),
(2, ' vidimo se', 'odobreno', 1, '2018-06-30 20:03:42', 1),
(3, ' vidimo se', 'odobreno', 1, '2018-06-30 20:07:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'ADMIN'),
(2, 'DRIVER'),
(3, 'PASSENGER');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `id` int(11) NOT NULL,
  `num_empty_seats` int(11) NOT NULL,
  `start_address` varchar(45) NOT NULL,
  `departue_time` datetime NOT NULL,
  `finish_address` varchar(45) NOT NULL,
  `driver` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`id`, `num_empty_seats`, `start_address`, `departue_time`, `finish_address`, `driver`, `created_at`) VALUES
(1, 2, 'Split, Vukovarska 8', '2018-07-13 00:00:00', 'Mostar, Vukovarska 74', 1, '2018-06-30 18:29:23'),
(2, 4, 'Split', '2018-06-28 21:12:11', 'Zagreb', 1, '2018-06-30 20:10:53'),
(4, 8, 'Split', '2018-07-28 21:12:11', 'Zagreb', 1, '2018-06-30 21:11:57'),
(5, 3, 'Mostar', '2018-07-03 21:12:11', 'Sarajevo', 3, '2018-07-01 14:42:59');

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `id` int(11) NOT NULL,
  `passenger` int(11) NOT NULL,
  `accepted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(45) DEFAULT NULL,
  `id_route` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` (`id`, `passenger`, `accepted`, `created_at`, `comment`, `id_route`) VALUES
(1, 2, 0, '2018-06-30 18:31:19', 'priijava', 1),
(3, 4, 0, '2018-07-01 14:35:33', 'Pozdrav, javljam se za prijevoz.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `contact_number` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `member_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `address` varchar(45) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `email`, `password`, `contact_number`, `id_role`, `active`, `member_since`, `address`, `token`) VALUES
(1, 'John', 'Smith', 'john.smith@gmail.com', 'johnJohn', 568956, 2, 1, '2018-06-30 18:17:11', NULL, NULL),
(2, 'Lea', 'Sarić', 'lea.saric@gmail.com', 'lealea', 23568974, 3, 1, '2018-06-30 18:17:57', NULL, NULL),
(3, 'Zora', 'Kek', 'zora.kek@gmail.com', 'zoraZora', 5689562, 2, 1, '2018-06-30 20:16:23', 'Mostar, Zadarska 4', NULL),
(4, 'Teo', 'Tim', 'teo.tim@gmail.com', 'teteo', 147852369, 3, 1, '2018-06-30 20:17:20', 'Mostar, Santiceva 7', NULL),
(5, 'Leona', 'Sea', 'leona.sea@gmail.com', 'leona', 5989898, 1, 1, '2018-06-30 20:18:15', 'Mostar', NULL),
(6, 'Theodor', 'Sh', 'theodor.s@gmail.com', 'theodor', 568956, 2, 1, '2018-07-01 15:52:09', 'Mostar', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `id_role`, `id_user`) VALUES
(1, 2, 1),
(2, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_message_trasnport` (`id_transport`),
  ADD KEY `fk_message_user_idx` (`sender`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_route_driver_idx` (`driver`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transport_user_idx` (`passenger`),
  ADD KEY `fk_transport_route_idx` (`id_route`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_user_role_idx` (`id_role`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_role_role_idx` (`id_role`),
  ADD KEY `fk_user_role_user_idx` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `route`
--
ALTER TABLE `route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `transport`
--
ALTER TABLE `transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_trasnport` FOREIGN KEY (`id_transport`) REFERENCES `transport` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_message_user` FOREIGN KEY (`sender`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `route`
--
ALTER TABLE `route`
  ADD CONSTRAINT `fk_route_driver` FOREIGN KEY (`driver`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transport`
--
ALTER TABLE `transport`
  ADD CONSTRAINT `fk_transport_route` FOREIGN KEY (`id_route`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transport_user` FOREIGN KEY (`passenger`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_user_role_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_role_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
