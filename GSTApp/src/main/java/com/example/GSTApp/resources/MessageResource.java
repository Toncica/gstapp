package com.example.GSTApp.resources;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.GSTApp.domain.Message;
import com.example.GSTApp.service.MessageService;

@RequestMapping("/messages")
@RestController
public class MessageResource {

	public static final Logger logger = LoggerFactory.getLogger(MessageResource.class);

	@Autowired
	MessageService messageService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Message>> getMessageByUser(Principal principal) {
		return messageService.getAllByUser(principal);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Message> getMessage(@PathVariable("id") int id, Principal principal) {
		return messageService.getOne(id, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Message> updateMessage(@PathVariable("id") int id, @RequestBody Message message,
			Principal principal) {
		return messageService.update(id, message, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRoute(@PathVariable("id") int id, Principal principal) {
		return messageService.delete(id, principal);
	}

}
