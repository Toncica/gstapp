package com.example.GSTApp.resources;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.GSTApp.domain.Route;
import com.example.GSTApp.domain.Transport;
import com.example.GSTApp.service.RouteService;
import com.example.GSTApp.service.TransportService;

@RequestMapping("/routes")
@RestController
public class RouteResource {

	public static final Logger logger = LoggerFactory.getLogger(RouteResource.class);

	@Autowired
	RouteService routeService;

	@Autowired
	TransportService transportService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Route>> getRoutesByUser(Principal principal) {
		return routeService.getAllByUser(principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Route> getRoute(@PathVariable("id") int idRoute, Principal principal) {
		return routeService.getRoute(idRoute, principal);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Route> createRoute(@RequestBody Route route, Principal principal) {
		return routeService.createRoute(route, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Route> updateRoute(@PathVariable("id") int idRoute, @RequestBody Route route,
			Principal principal) {
		return routeService.updateRoute(idRoute, route, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRoute(@PathVariable("id") int idRoute, Principal principal) {
		return routeService.deleteRoute(idRoute, principal);
	}

	@RequestMapping(value = "/{id}/transports", method = RequestMethod.POST)
	public ResponseEntity<Transport> createTransport(@PathVariable("id") int idRoute, @RequestBody Transport transport,
			Principal principal) {
		return transportService.createTransport(idRoute, transport, principal);
	}

}
