package com.example.GSTApp.resources;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.GSTApp.domain.Message;
import com.example.GSTApp.domain.Transport;
import com.example.GSTApp.domain.TransportAcceptingOnly;
import com.example.GSTApp.service.MessageService;
import com.example.GSTApp.service.TransportService;

@RequestMapping("/transports")
@RestController
public class TransportResource {

	public static final Logger logger = LoggerFactory.getLogger(TransportResource.class);

	@Autowired
	TransportService transportService;

	@Autowired
	MessageService messageService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Transport>> getTransportsByUser(Principal principal) {
		return transportService.getAllByUser(principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Transport> getTransport(@PathVariable("id") int transportId, Principal principal) {
		return transportService.getTransport(transportId, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<Transport> updateTransportAccepting(@PathVariable("id") int transportId,
			@RequestBody TransportAcceptingOnly transport, Principal principal) {
		return transportService.updateAccepting(transportId, transport, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Transport> updateTransport(@PathVariable("id") int transportId,
			@RequestBody Transport transport, Principal principal) {
		return transportService.updateTransport(transportId, transport, principal);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> updateTransport(@PathVariable("id") int transportId, Principal principal) {
		return transportService.deleteTransport(transportId, principal);
	}

	@RequestMapping(value = "/{id}/messages", method = RequestMethod.POST)
	public ResponseEntity<Message> createMessage(@PathVariable("id") int idTransport, @RequestBody Message message,
			Principal principal) {
		return messageService.create(idTransport, message, principal);
	}

}
