package com.example.GSTApp.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = { "createdAt" }, allowGetters = true)
public class Route implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "num_empty_seats")
	private int numEmptySeats;

	@Column(name = "start_address")
	private String startAddress;

	@Column(name = "departue_time")
	private Date departueTime;

	@Column(name = "finish_address")
	private String finishAddress;

	@Column(nullable = false, updatable = false, name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driver", nullable = false)
	private User driver;

	public Route() {
	}

	public Route(int id, int numEmptySeats, String startAddress, Date departueTime, String finishAddress,
			Date createdAt, User driver) {
		this.id = id;
		this.numEmptySeats = numEmptySeats;
		this.startAddress = startAddress;
		this.departueTime = departueTime;
		this.finishAddress = finishAddress;
		this.createdAt = createdAt;
		this.driver = driver;
	}

	public int getId() {
		return id;
	}

	public int getNumEmptySeats() {
		return numEmptySeats;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public Date getDepartueTime() {
		return departueTime;
	}

	public String getFinishAddress() {
		return finishAddress;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	@JsonIgnore
	public User getDriver() {
		return driver;
	}

	public void setDriver(User driver) {
		this.driver = driver;
	}

	@Override
	public String toString() {
		return "Route [id=" + id + ", numEmptySeats=" + numEmptySeats + ", startAddress=" + startAddress
				+ ", departueTime=" + departueTime + ", finishAddress=" + finishAddress + ", createdAt=" + createdAt
				+ ", driver=" + driver + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((departueTime == null) ? 0 : departueTime.hashCode());
		result = prime * result + ((driver == null) ? 0 : driver.hashCode());
		result = prime * result + ((finishAddress == null) ? 0 : finishAddress.hashCode());
		result = prime * result + id;
		result = prime * result + numEmptySeats;
		result = prime * result + ((startAddress == null) ? 0 : startAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Route other = (Route) obj;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (departueTime == null) {
			if (other.departueTime != null)
				return false;
		} else if (!departueTime.equals(other.departueTime))
			return false;
		if (driver == null) {
			if (other.driver != null)
				return false;
		} else if (!driver.equals(other.driver))
			return false;
		if (finishAddress == null) {
			if (other.finishAddress != null)
				return false;
		} else if (!finishAddress.equals(other.finishAddress))
			return false;
		if (id != other.id)
			return false;
		if (numEmptySeats != other.numEmptySeats)
			return false;
		if (startAddress == null) {
			if (other.startAddress != null)
				return false;
		} else if (!startAddress.equals(other.startAddress))
			return false;
		return true;
	}

	public Route updateRoute(Route model) {
		this.setDepartueTime(model.getDepartueTime());
		this.setFinishAddress(model.getFinishAddress());
		this.setNumEmptySeats(model.getNumEmptySeats());
		this.setStartAddress(model.getStartAddress());
		return this;
	}

	private void setNumEmptySeats(int numEmptySeats) {
		this.numEmptySeats = numEmptySeats;
	}

	private void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	private void setFinishAddress(String finishAddress) {
		this.finishAddress = finishAddress;
	}

	private void setDepartueTime(Date departueTime) {
		this.departueTime = departueTime;
	}

}
