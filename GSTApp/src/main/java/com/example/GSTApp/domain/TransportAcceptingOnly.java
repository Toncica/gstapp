package com.example.GSTApp.domain;

public class TransportAcceptingOnly {

	public TransportAcceptingOnly() {

	}

	public TransportAcceptingOnly(int id, boolean accepted) {
		this.id = id;
		this.accepted = accepted;
	}

	public int getId() {
		return id;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	@Override
	public String toString() {
		return "TransportAcceptingOnly [id=" + id + ", accepted=" + accepted + "]";
	}

	private int id;
	private boolean accepted;

}
