package com.example.GSTApp.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = { "createdAt" }, allowGetters = true)
public class Transport implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "comment")
	private String comment;

	@Column(name = "accepted")
	private boolean accepted;

	@Column(nullable = false, updatable = false, name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_route", nullable = false)
	private Route idRoute;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "passenger", nullable = false)
	private User passenger;

	public Transport() {

	}

	public Transport(int id, String comment, boolean accepted, Date createdAt, Route idRoute, User passenger) {
		this.id = id;
		this.comment = comment;
		this.accepted = accepted;
		this.createdAt = createdAt;
		this.idRoute = idRoute;
		this.passenger = passenger;
	}

	public int getId() {
		return id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Route getIdRoute() {
		return idRoute;
	}

	public void setIdRoute(Route idRoute) {
		this.idRoute = idRoute;
	}

	public User getPassenger() {
		return passenger;
	}

	public void setPassenger(User passenger) {
		this.passenger = passenger;
	}
}
