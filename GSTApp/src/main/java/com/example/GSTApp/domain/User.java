package com.example.GSTApp.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false, name = "contact_number")
	private int contactNumber;

	@Column(nullable = false, name = "name")
	private String name;

	@Column(nullable = false, name = "lastname")
	private String lastname;

	@Email
	@Column(nullable = false, name = "email")
	private String email;

	@Column(nullable = false, name = "password")
	private String password;

	@Column(name = "address")
	private String address;

	@Column(name = "token")
	private String token;

	@Column(nullable = false, name = "active")
	private int active;

	@Column(nullable = false, updatable = false, name = "member_since")
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date memberSince;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_role", nullable = false)
	private Role roles;

	@Transient
	private int roleId;

	public User() {

	}

	public User(String firstName, String lastName) {
		this.name = firstName;
		this.lastname = lastName;
	}

	public User(User user) {
		this.contactNumber = user.contactNumber;
		this.name = user.name;
		this.lastname = user.lastname;
		this.email = user.email;
		this.password = user.password;
		this.address = user.address;
		this.token = user.token;
		this.active = user.active;
		this.memberSince = user.memberSince;
		this.roles = user.roles;
	}

	public int getId() {
		return id;
	}

	public int getContactNumber() {
		return contactNumber;
	}

	public String getName() {
		return name;
	}

	public String getLastname() {
		return lastname;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getAddress() {
		return address;
	}

	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Date getMemberSince() {
		return memberSince;
	}

	public Role getRole() {
		return roles;
	}

	public void setRoles(Role roles) {
		this.roles = roles;
	}

	public String getFullName() {
		return this.name + ' ' + this.lastname;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", contactNumber=" + contactNumber + ", name=" + name + ", lastname=" + lastname
				+ ", email=" + email + ", password=" + password + ", address=" + address + ", token=" + token
				+ ", active=" + active + ", memberSince=" + memberSince + ", roles=" + roles + ", roleId=" + roleId
				+ "]";
	}

	public boolean isAdmin() {
		return this.getRole().getId() == 1;
	}

	public boolean isDriver() {
		return this.getRole().getId() == 2;
	}

	public boolean isPassenger() {
		return this.getRole().getId() == 3;
	}

	public boolean isOwner(int id) {
		return this.getId() == id;
	}

	public boolean isOwnerOrAdmin(int id) {
		return (this.getId() == id || isAdmin());
	}

	public boolean isOwnerAndDriver(int id) {
		return this.getId() == id && isDriver();
	}

	public boolean isOwnerAndPassenger(int id) {
		return this.getId() == id && isPassenger();
	}
	
	public boolean isPassengerrOrDriver() {
		return (isDriver() || isPassenger());
	}

	public User updateUser(User model) {
		this.setAddress(model.getAddress());
		this.setActive(model.getActive());
		this.setEmail(model.getEmail());
		this.setLastname(model.getLastname());
		this.setName(model.getName());
		this.setPassword(model.getPassword());
		this.setContactNumber(model.getContactNumber());
		return this;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int idRole) {
		this.roleId = idRole;
	}
	
	

	private void setContactNumber(int contactNumber) {
		this.contactNumber = contactNumber;
	}

	private void setName(String name) {
		this.name = name;
	}

	private void setLastname(String lastname) {
		this.lastname = lastname;
	}

	private void setPassword(String password) {
		this.password = password;
	}

	private void setEmail(String email) {
		this.email = email;
	}

	private void setAddress(String address) {
		this.address = address;
	}

}
