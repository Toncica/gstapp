package com.example.GSTApp.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.GSTApp.domain.Route;
import com.example.GSTApp.domain.Transport;
import com.example.GSTApp.domain.TransportAcceptingOnly;
import com.example.GSTApp.domain.User;
import com.example.GSTApp.domain.Message;
import com.example.GSTApp.repository.MessageRepository;
import com.example.GSTApp.repository.RouteRepository;
import com.example.GSTApp.repository.TransportRepository;
import com.example.GSTApp.repository.UserRepository;

@Service
public class TransportService {

	private TransportRepository transportRepository;
	private UserRepository userRepository;
	private RouteRepository routeRepository;
	private MessageRepository messageRepository;

	@Autowired
	public TransportService(TransportRepository transportRepository, UserRepository userRepository,
			RouteRepository routeRepository, MessageRepository messageRepository) {
		this.transportRepository = transportRepository;
		this.userRepository = userRepository;
		this.routeRepository = routeRepository;
		this.messageRepository = messageRepository;
	}

	public ResponseEntity<List<Transport>> getAllByUser(Principal principal) {
		List<Transport> response = new ArrayList<>();
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		if (user.isAdmin()) {
			return new ResponseEntity<>((List<Transport>) transportRepository.findAll(), HttpStatus.OK);
		} else if (user.isPassenger()) {
			return new ResponseEntity<>(transportRepository.findByPassenger(user), HttpStatus.OK);
		} else if (user.isDriver()) {
			List<Route> routes = routeRepository.findByDriver(user);
			if (routes.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			} else {
				for (Route route : routes) {
					List<Transport> transports = transportRepository.findByIdRoute(route);
					if (!transports.isEmpty()) {
						for (Transport transport : transports) {
							response.add(transport);
						}
					}
				}
				return new ResponseEntity(response, HttpStatus.OK);
			}
		}

		return null;
	}

	public ResponseEntity<Transport> getTransport(int id, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		Optional<Transport> transportOptional = transportRepository.findById(id);
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		if (!transportOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(transportOptional.get(), HttpStatus.OK);
	}

	public ResponseEntity<Transport> createTransport(int idRoute, Transport transport, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Route> routeOptional = routeRepository.findByIdAndDepartueTimeAfter(idRoute, new Date());
		if (!routeOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (!user.isPassenger()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		Route route = routeOptional.get();
		transport.setAccepted(false);
		transport.setIdRoute(route);
		transport.setPassenger(user);

		return new ResponseEntity<>(transportRepository.save(transport), HttpStatus.CREATED);
	}

	public ResponseEntity<Transport> updateAccepting(int transportId, TransportAcceptingOnly model,
			Principal principal) {

		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Transport> transportOptional = transportRepository.findById(transportId);
		if (!transportOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		Transport transport = transportOptional.get();

		Optional<Route> routeOptional = routeRepository.findByIdAndDepartueTimeAfter(transport.getIdRoute().getId(),
				new Date());
		if (!routeOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Route route = routeOptional.get();

		if (!user.isOwnerAndDriver(route.getDriver().getId())) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		transport.setAccepted(model.isAccepted());
		return new ResponseEntity<>(transportRepository.save(transport), HttpStatus.OK);
	}

	public ResponseEntity<Transport> updateTransport(int transportId, Transport model, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Transport> transportOptional = transportRepository.findById(transportId);
		if (!transportOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		Transport transport = transportOptional.get();

		if (!user.isOwnerAndPassenger(transport.getPassenger().getId())) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		transport.setComment(model.getComment());
		return new ResponseEntity<>(transportRepository.save(transport), HttpStatus.OK);
	}

	public ResponseEntity<?> deleteTransport(int transportId, Principal principal) {
		Optional<Transport> transportOptional = transportRepository.findById(transportId);
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		if (!transportOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}

		Transport transport = transportOptional.get();
		User user = userOptional.get();
		if (!user.isOwnerAndPassenger(transport.getPassenger().getId())) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}
		List<Message> messages = messageRepository.findByIdTransport(transport);
		if (!messages.isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		transportRepository.delete(transport);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}

}
