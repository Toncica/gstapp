package com.example.GSTApp.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.GSTApp.domain.Route;
import com.example.GSTApp.domain.Transport;
import com.example.GSTApp.domain.User;
import com.example.GSTApp.repository.RouteRepository;
import com.example.GSTApp.repository.TransportRepository;
import com.example.GSTApp.repository.UserRepository;

@Service
public class RouteService {

	private RouteRepository routeRepository;
	private UserRepository userRepository;
	private TransportRepository transportRepository;

	@Autowired
	public RouteService(RouteRepository routeRepository, UserRepository userRepository,
			TransportRepository transportRepository) {
		this.routeRepository = routeRepository;
		this.userRepository = userRepository;
		this.transportRepository = transportRepository;
	}

	public ResponseEntity<List<Route>> getAllByUser(Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		if (user.isDriver()) {
			return new ResponseEntity(routeRepository.findByDriver(user), HttpStatus.OK);
		} else {
			return new ResponseEntity((List<Route>) routeRepository.findAll(), HttpStatus.OK);
		}
	}

	public ResponseEntity<Route> getRoute(int id, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		Optional<Route> routeOptional = routeRepository.findById(id);
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		if (!routeOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(routeOptional.get(), HttpStatus.OK);
	}

	public ResponseEntity<Route> createRoute(Route route, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		User user = userOptional.get();
		if (!user.isDriver()) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}
		route.setDriver(user);
		return new ResponseEntity(routeRepository.save(route), HttpStatus.CREATED);
	}

	public ResponseEntity<Route> updateRoute(int id, Route model, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Route> routeOptional = routeRepository.findById(id);
		if (!routeOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}

		Route route = routeOptional.get();
		if (!user.isOwnerAndDriver(route.getDriver().getId())) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}

		return new ResponseEntity(routeRepository.save(route.updateRoute(model)), HttpStatus.OK);
	}

	public ResponseEntity<?> deleteRoute(int id, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Route> routeOptional = routeRepository.findById(id);
		if (!routeOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		Route route = routeOptional.get();

		if (user.isOwnerAndDriver(route.getDriver().getId())) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}

		List<Transport> transports = transportRepository.findByIdRoute(route);
		if (!transports.isEmpty()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		
		routeRepository.delete(route);
		return new ResponseEntity(HttpStatus.NO_CONTENT);

	}
}
