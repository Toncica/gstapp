package com.example.GSTApp.service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.GSTApp.domain.Role;
import com.example.GSTApp.domain.User;
import com.example.GSTApp.repository.RoleRepository;
import com.example.GSTApp.repository.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository;
	private RoleRepository roleRepository;

	@Autowired
	public UserService(UserRepository userRepository, RoleRepository roleRepository) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}

	public ResponseEntity<List<User>> getAllUsers(Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		if (!user.isAdmin()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<>((List<User>) userRepository.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<User> getUser(int id, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		if (!user.isOwnerOrAdmin(id)) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		Optional<User> userGetOptional = userRepository.findById(id);
		if (!userGetOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(userGetOptional.get(), HttpStatus.OK);

	}

	public ResponseEntity<User> createByAdmin(User model, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		if (!user.isAdmin()) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}
		
		Optional<Role> roleOptional = roleRepository.findById(model.getRoleId());

		if (!roleOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

		model.setRoles(roleOptional.get());
		model.setActive(1);
		return new ResponseEntity(userRepository.save(model), HttpStatus.CREATED);
	}

	public ResponseEntity<User> create(User model) {
		Optional<Role> roleOptional = roleRepository.findById(model.getRoleId());

		if (!roleOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		Role role = roleOptional.get();

		if (role.isAdmin()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

		model.setRoles(role);
		model.setActive(1);
		return new ResponseEntity(userRepository.save(model), HttpStatus.CREATED);
	}

	public ResponseEntity<User> update(int idUser, User model, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<User> userUpdOptional = userRepository.findById(idUser);
		if (!userUpdOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		User userUpd = userUpdOptional.get();

		if (!user.isOwnerOrAdmin(idUser)) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity(userRepository.save(userUpd.updateUser(model)), HttpStatus.OK);

	}

	public ResponseEntity<User> deActivate(int idUser, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<User> userDelOptional = userRepository.findById(idUser);
		if (!userDelOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		User userDel = userDelOptional.get();

		if (!user.isOwnerOrAdmin(idUser)) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}
		userDel.setActive(0);
		return new ResponseEntity(userRepository.save(userDel), HttpStatus.OK);
	}

}
