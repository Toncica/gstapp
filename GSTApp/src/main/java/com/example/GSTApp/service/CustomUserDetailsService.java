package com.example.GSTApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.GSTApp.domain.CustomUserDetails;
import com.example.GSTApp.domain.User;
import com.example.GSTApp.repository.UserRepository;

@Service
public class CustomUserDetailsService  implements UserDetailsService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		 java.util.Optional<User> optionalUser = userRepository.findByEmail(username);

	        optionalUser.orElseThrow(() -> new UsernameNotFoundException("Username not found"));
	        return optionalUser.map(CustomUserDetails::new).get();
	}

}
