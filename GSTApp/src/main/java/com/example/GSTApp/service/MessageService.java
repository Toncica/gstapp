package com.example.GSTApp.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.GSTApp.domain.Message;
import com.example.GSTApp.domain.Route;
import com.example.GSTApp.domain.Transport;
import com.example.GSTApp.domain.User;
import com.example.GSTApp.repository.MessageRepository;
import com.example.GSTApp.repository.RouteRepository;
import com.example.GSTApp.repository.TransportRepository;
import com.example.GSTApp.repository.UserRepository;

@Service
public class MessageService {

	private MessageRepository messageRepository;
	private TransportRepository transportRepository;
	private UserRepository userRepository;
	private RouteRepository routeRepository;

	@Autowired
	public MessageService(TransportRepository transportRepository, UserRepository userRepository,
			RouteRepository routeRepository, MessageRepository messageRepository) {
		this.transportRepository = transportRepository;
		this.userRepository = userRepository;
		this.routeRepository = routeRepository;
		this.messageRepository = messageRepository;
	}

	public ResponseEntity<List<Message>> getAllByUser(Principal principal) {
		List<Message> response = new ArrayList<>();
		List<Transport> transports = new ArrayList<>();
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		if (user.isAdmin()) {
			return new ResponseEntity((List<Message>) messageRepository.findAll(), HttpStatus.OK);
		} else if (user.isDriver()) {
			List<Route> routes = routeRepository.findByDriver(user);
			if (routes.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			} else {
				for (Route route : routes) {
					List<Transport> transportsTempDriver = transportRepository.findByIdRoute(route);
					if (!transportsTempDriver.isEmpty()) {
						for (Transport transport : transportsTempDriver) {
							transports.add(transport);
						}
					}
				}
			}
		} else if (user.isPassenger()) {
			List<Transport> transportsTempPassenger = transportRepository.findByPassenger(user);
			if (transportsTempPassenger.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			} else {
				for (Transport transport : transportsTempPassenger) {
					transports.add(transport);
				}
			}
		}

		if (transports.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		} else {
			for (Transport transportAll : transports) {
				List<Message> allMessage = messageRepository.findByIdTransport(transportAll);
				if (!allMessage.isEmpty()) {
					for (Message oneMessage : allMessage) {
						response.add(oneMessage);
					}
				}
			}
			return new ResponseEntity(response, HttpStatus.OK);
		}

	}

	public ResponseEntity<Message> getOne(int id, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Message> messageOptional = messageRepository.findById(id);
		if (!messageOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(messageOptional.get(), HttpStatus.OK);

	}

	public ResponseEntity<Message> create(int idTransport, Message message, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Transport> transportOptional = transportRepository.findById(idTransport);

		if (!transportOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		Transport transport = transportOptional.get();
		if (!transport.isAccepted()) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

		if (!user.isPassengerrOrDriver()) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		} else if (user.isPassenger()) {
			if (!user.isOwner(transport.getPassenger().getId())) {
				return new ResponseEntity(HttpStatus.FORBIDDEN);
			}
		} else if (user.isDriver()) {
			if (!user.isOwner(transport.getIdRoute().getDriver().getId())) {
				return new ResponseEntity(HttpStatus.FORBIDDEN);
			}
		}

		message.setIdTransport(transport);
		message.setSender(user);
		return new ResponseEntity(messageRepository.save(message), HttpStatus.CREATED);
	}

	public ResponseEntity<Message> update(int idMessage, Message model, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Message> messageOptional = messageRepository.findById(idMessage);
		if (!messageOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		Message message = messageOptional.get();

		if (!user.isOwner(message.getSender().getId())) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}

		message.setMessage(model.getMessage());
		return new ResponseEntity(messageRepository.save(message), HttpStatus.OK);
	}

	public ResponseEntity<?> delete(int idMessage, Principal principal) {
		Optional<User> userOptional = userRepository.findByEmail(principal.getName());
		if (!userOptional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		User user = userOptional.get();

		Optional<Message> messageOptional = messageRepository.findById(idMessage);
		if (!messageOptional.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		Message message = messageOptional.get();

		if (!user.isOwner(message.getSender().getId())) {
			return new ResponseEntity(HttpStatus.FORBIDDEN);
		}
		messageRepository.delete(message);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}

}
