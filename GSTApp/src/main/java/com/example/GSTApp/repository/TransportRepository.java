package com.example.GSTApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.GSTApp.domain.Route;
import com.example.GSTApp.domain.Transport;
import com.example.GSTApp.domain.User;

public interface TransportRepository extends CrudRepository<Transport, Integer>{

	List<Transport> findByPassenger(User user);

	List<Transport> findByIdRoute(Route route);

	
}
