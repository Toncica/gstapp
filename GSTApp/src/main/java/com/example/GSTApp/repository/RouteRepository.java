package com.example.GSTApp.repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.GSTApp.domain.Route;
import com.example.GSTApp.domain.User;

public interface RouteRepository extends CrudRepository<Route, Integer> {

	List<Route> findByDriver(User user);

	@Query("SELECT r FROM Route r where r.id = ?1 AND r.departueTime > ?2")
	public Optional<Route> findByIdAndDepartueTimeAfter(int id, Date currentDate);
	
	public List<Route> findByDepartueTimeAfter(Date currentDate);

}
