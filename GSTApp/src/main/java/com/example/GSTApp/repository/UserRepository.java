package com.example.GSTApp.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.GSTApp.domain.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	
    Optional<User> findByEmail(String username);
    
    User findByEmailAndPassword(String username, String password);

}
