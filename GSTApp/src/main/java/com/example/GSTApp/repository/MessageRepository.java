package com.example.GSTApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.GSTApp.domain.Message;
import com.example.GSTApp.domain.Transport;

public interface MessageRepository extends CrudRepository<Message, Integer> {

	List<Message> findByIdTransport(Transport transportAll);

}
