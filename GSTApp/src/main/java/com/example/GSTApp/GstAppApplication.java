package com.example.GSTApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
public class GstAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GstAppApplication.class, args);
	}
	
	@RestController
	@RequestMapping("/api")
	public class ApiController{
		@GetMapping("/greeting")
		public String getGreeting() {
			return "Hello world from API";
		}
	}
}
