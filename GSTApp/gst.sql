SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `getsettransport` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `getsettransport` ;

-- -----------------------------------------------------
-- Table `getsettransport`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `getsettransport`.`role` ;

CREATE TABLE IF NOT EXISTS `getsettransport`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'ADMIN'),
(2, 'DRIVER'),
(3, 'PASSENGER');


-- -----------------------------------------------------
-- Table `getsettransport`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `getsettransport`.`user` ;

CREATE TABLE IF NOT EXISTS `getsettransport`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `contact_number` INT NOT NULL,
  `id_role` INT NOT NULL,
  `active` TINYINT NOT NULL DEFAULT 1,
  `member_since` TIMESTAMP NOT NULL,
  `address` VARCHAR(45) NULL,
  `token` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_user_role_idx` (`id_role` ASC),
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`id_role`)
    REFERENCES `getsettransport`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `getsettransport`.`route`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `getsettransport`.`route` ;

CREATE TABLE IF NOT EXISTS `getsettransport`.`route` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `num_empty_seats` INT NOT NULL,
  `start_address` VARCHAR(45) NOT NULL,
  `departue_time` DATETIME NOT NULL,
  `finish_address` VARCHAR(45) NOT NULL,
  `driver` INT NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_route_driver_idx` (`driver` ASC),
  CONSTRAINT `fk_route_driver`
    FOREIGN KEY (`driver`)
    REFERENCES `getsettransport`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `getsettransport`.`transport`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `getsettransport`.`transport` ;

CREATE TABLE IF NOT EXISTS `getsettransport`.`transport` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `passenger` INT NOT NULL,
  `accepted` TINYINT NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL,
  `comment` VARCHAR(45) NULL,
  `id_route` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transport_user_idx` (`passenger` ASC),
  INDEX `fk_transport_route_idx` (`id_route` ASC),
  CONSTRAINT `fk_transport_user`
    FOREIGN KEY (`passenger`)
    REFERENCES `getsettransport`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transport_route`
    FOREIGN KEY (`id_route`)
    REFERENCES `getsettransport`.`route` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `getsettransport`.`message`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `getsettransport`.`message` ;

CREATE TABLE IF NOT EXISTS `getsettransport`.`message` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message` LONGTEXT NOT NULL,
  `title` VARCHAR(45) NULL,
  `id_transport` INT NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  `sender` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_message_trasnport` (`id_transport` ASC),
  INDEX `fk_message_user_idx` (`sender` ASC),
  CONSTRAINT `fk_message_trasnport`
    FOREIGN KEY (`id_transport`)
    REFERENCES `getsettransport`.`transport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_user`
    FOREIGN KEY (`sender`)
    REFERENCES `getsettransport`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `getsettransport`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `getsettransport`.`user_role` ;

CREATE TABLE IF NOT EXISTS `getsettransport`.`user_role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_role` INT NOT NULL,
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_role_role_idx` (`id_role` ASC),
  INDEX `fk_user_role_user_idx` (`id_user` ASC),
  CONSTRAINT `fk_user_role_role`
    FOREIGN KEY (`id_role`)
    REFERENCES `getsettransport`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_role_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `getsettransport`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
